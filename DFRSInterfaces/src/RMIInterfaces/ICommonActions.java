/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Common RMI functions
 */
package RMIInterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;


public interface ICommonActions extends Remote {
    public CommandResult<ArrayList<Flight>> GetFlights() throws RemoteException;
}
