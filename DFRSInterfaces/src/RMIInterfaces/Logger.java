/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Logs simple message to a text file
 */

package RMIInterfaces;

import java.io.File;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import org.joda.time.DateTime;

public class Logger {

    public static Logger GlobalInstance;
    
    private String _src; //The source of the logging (example :  Server's name)
    private Writer _out; //File to write to

    public Logger(String src, String destinationFolder) {
        _src = src;

        File dir = new File("Log/" + destinationFolder);
        dir.mkdirs();

        File logFile = null;
        try {
            logFile = new File(dir.getPath() + "/" + _src + ".txt");
            logFile.createNewFile();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        try {
            _out = Files.newBufferedWriter(Paths.get(logFile.getPath()), StandardOpenOption.APPEND);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    public synchronized void Log(String from, String message) {
        try {
            _out.write("------------------------------------\r\n");
            _out.write(String.format("Timestamp : %s%n", DateTime.now().toString("yyyy-MM-dd h:mm a")));
            _out.write(String.format("Source : %s%n", _src));
            _out.write(String.format("From : %s%n", from));
            _out.write("************************************\r\n\r\n");
            _out.write(message + "\r\n\r\n");
            _out.write("------------------------------------\r\n\r\n");
            _out.flush();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }

    }

}
