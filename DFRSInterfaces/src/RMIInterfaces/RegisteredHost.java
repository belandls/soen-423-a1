/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Represents a UDP Host with it's listening port (used for the UDP registry)
 */

package RMIInterfaces;

import java.io.Serializable;


public class RegisteredHost implements Serializable{
    
    private String _host;
    private int _port;
    
    public RegisteredHost(String host, int port){
        _host = host;
        _port = port;
    }
    
    public String GetHost(){
        return _host;
    }
    
    public int GetPort(){
        return _port;
    }
    
}
