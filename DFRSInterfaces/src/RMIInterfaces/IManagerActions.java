/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Manager RMI functions
 */
package RMIInterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IManagerActions extends Remote {
    public CommandResult<String> GetBookedFlightCount(ManagerParameter<RecordType> type) throws RemoteException;
    public CommandResult<Flight> EditFlightRecord(ManagerParameter<Integer> recordId, FieldName fn, String value) throws RemoteException;
}
