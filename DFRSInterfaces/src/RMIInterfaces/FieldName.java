/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Field names for editRecord
 */
package RMIInterfaces;


public enum FieldName {
    DESTINATION,
    DATE,
    ECON_SEATS,
    BUSINESS_SEATS,
    FIT_SEATS,
    CREATE,
    DELETE
}
