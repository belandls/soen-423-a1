/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Record type to fetch when querying the booked count
 */
package RMIInterfaces;


public enum RecordType {
    NONE, //Just to get a default value
    ALL_BOOKED_FLIGHT,
    ECON_BOOKED_FLIGHT,
    BUSINESS_BOOKED_FLIGHT,
    FIT_BOOKED_FLIGHT
    
}
