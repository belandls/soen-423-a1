/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Passenger RMI functions
 */
package RMIInterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import org.joda.time.DateTime;


public interface IPassengerActions extends Remote {
    public Flight Test() throws RemoteException; //test function
    public CommandResult<Object> BookFlight(String fn, String ln, String addr, String phone, String dest, DateTime date, SeatClass sc) throws RemoteException;
}
