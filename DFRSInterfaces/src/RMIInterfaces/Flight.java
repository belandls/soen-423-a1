/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Represents a bookable flight
 */

package RMIInterfaces;

import java.io.Serializable;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class Flight implements Serializable {

    private final static DateTimeFormatter _Dt_Formatter = DateTimeFormat.forPattern("yyyy-MM-dd h:mm a");
    private static int _Record_Id_Counter = 0;

    private final int _record_id;
    private String _departure;
    //volatile used to ensure proper values in the ToString()
    private volatile String _destination;
    private volatile DateTime _date;
    private volatile int _total_economy_seats;
    private volatile int _total_business_seats;
    private volatile int _total_fit_seats;
    private volatile int _taken_economy_seats;
    private volatile int _taken_business_seats;
    private volatile int _taken_fit_seats;

    public Flight(String departure, String dest, DateTime date, int econSeats, int busSeats, int fitSeats) {
        _departure = departure;
        _destination = dest;
        _date = date;
        _total_economy_seats = econSeats;
        _total_business_seats = busSeats;
        _total_fit_seats = fitSeats;
        _record_id = ++_Record_Id_Counter;
    }

    public boolean Equals(Flight f) {
        return _record_id == f._record_id;
    }

    public boolean Equals(String dest, DateTime date) {
        return dest.equals(_destination) && date.equals(_date);
    }

    public String UpdateRecord(FieldName fn, String value) {
        //only 1 update at the time is possible
        synchronized (this) {
            int intVal = -1;
            switch (fn) {
                case DESTINATION:
                    //hardcoded for simplicity
                    if (value.equals("WSH") || value.equals("ND") || value.equals("MTL")) {
                        _destination = value;
                    }else{
                        return "Invalid destination (" + value + ")";
                    }
                    break;
                case DATE:
                    try {
                        _date = _Dt_Formatter.parseDateTime(value);
                    } catch (Exception ex) {
                        return ex.getMessage();
                    }
                    break;
                case ECON_SEATS:
                    intVal = Integer.parseInt(value);
                    if (intVal < _taken_economy_seats) {
                        return "Can't reduce the amount of ECON seats to less than the currently booked amount.";
                    } else if (intVal < 0) {
                        return "Negative number of seats are invalid";
                    }
                    _total_economy_seats = intVal;
                    break;
                case BUSINESS_SEATS:
                    intVal = Integer.parseInt(value);
                    if (intVal < _taken_business_seats) {
                        return "Can't reduce the amount of BUSINESS seats to less than the currently booked amount.";
                    } else if (intVal < 0) {
                        return "Negative number of seats are invalid";
                    }
                    _total_economy_seats = intVal;
                    break;
                case FIT_SEATS:
                    intVal = Integer.parseInt(value);
                    if (intVal < _taken_fit_seats) {
                        return "Can't reduce the amount of FIT seats to less than the currently booked amount.";
                    } else if (intVal < 0) {
                        return "Negative number of seats are invalid";
                    }
                    _total_fit_seats = intVal;
                    break;
            }
            return null;
        }
    }

    //Check if seats are available to book for a specific seat class.
    //If available, take it
    public boolean TryToTakeSeat(SeatClass sc) {
        synchronized (this) {
            if (null != sc) {
                switch (sc) {
                    case BUSINESS:
                        if (_taken_business_seats < _total_business_seats) {
                            ++_taken_business_seats;
                            return true;
                        }
                        break;
                    case FIT:
                        if (_taken_fit_seats < _total_fit_seats) {
                            ++_taken_fit_seats;
                            return true;
                        }
                        break;
                    case ECON:
                        if (_taken_economy_seats < _total_economy_seats) {
                            ++_taken_economy_seats;
                            return true;
                        }
                        break;
                    default:
                        break;
                }
            }
            return false;
        }
    }

    public int GetRecordId() {
        return _record_id;
    }

    @Override
    public String toString() {
        return String.format("[ID:%d] %s -> %s %s [e:%d/%d, b:%d/%d, f:%d/%d]", _record_id, _departure, _destination, _Dt_Formatter.print(_date), _taken_economy_seats, _total_economy_seats, _taken_business_seats, _total_business_seats, _taken_fit_seats, _total_fit_seats);
    }
}
