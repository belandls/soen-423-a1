/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Expands the information that id returned by the RMI functions
 */

package RMIInterfaces;

import java.io.Serializable;

public class CommandResult<T> implements Serializable {
    private final boolean _is_succes;
    private final String _message;
    private final T _result;
    
    public CommandResult(boolean isSuccess, String message, T result){
        _is_succes = isSuccess;
        _message = message;
        _result = result;
    }
    
    public boolean GetIsSuccess(){
        return this._is_succes;
    }
    
    public String GetMessage(){
        return this._message;
    }
    
    public T GetResutlt(){
        return this._result;
    }
}
