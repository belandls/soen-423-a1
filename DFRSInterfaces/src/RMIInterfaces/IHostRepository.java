/*
 * Author : Samuel Beland-Leblanc
 * Purpose : UDP Repository RMI functions
 */
package RMIInterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface IHostRepository extends Remote {

    public void Register(String city, int port) throws RemoteException;

    public RegisteredHost GetHostInformation(String targetCity) throws RemoteException;

    public ArrayList<RegisteredHost> GetAllRegisteredHostExceptCalling(String callingCity) throws RemoteException;
}
