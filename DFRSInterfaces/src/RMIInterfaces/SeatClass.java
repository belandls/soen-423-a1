/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Available seat classes
 */
package RMIInterfaces;

public enum SeatClass {
    ECON,
    BUSINESS,
    FIT
}
