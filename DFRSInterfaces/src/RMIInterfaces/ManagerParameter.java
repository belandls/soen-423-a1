/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Combine the manager's id with any other parameter that has to be passed through RMI functions
 */
package RMIInterfaces;

import java.io.Serializable;

public class ManagerParameter<T> implements Serializable {
    private String _manager_id;
    private T _param_value;
    
    public ManagerParameter(String manId, T value){
        _manager_id = manId;
        _param_value = value;
    }
    
    public String GetManagerId(){
        return _manager_id;
    }
    
    public T GetParameterValue(){
        return _param_value;
    }
    
}
