/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Simple interface launch the main interface the same way whether it's a passenger or a manager
 */
package dfrsclient;

public interface IUser {
    public void MainInterfaceLoop();
}
