/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Stores the manager ID in seperate form (city + number part) and parses id's
 */
package dfrsclient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ManagerId {

    public static ManagerId GetFromFullId(String fullId) {

        //Use Regex to extract City and number part (and to test if valid)
        Pattern p = Pattern.compile("([a-zA-Z]+)(\\d{4})");
        Matcher m = p.matcher(fullId);
        if (m.find()) {
            //Harcoded cities for simplicity
            if (m.group(1).equals("MTL") || m.group(1).equals("WSH") || m.group(1).equals("ND")) {
                ManagerId temp = new ManagerId(m.group(1), m.group(2));
                return temp;
            }

        }
        return null;
    }

    private String _city;
    private String _id_num;

    private ManagerId(String city, String idNum) {
        _city = city;
        _id_num = idNum;
    }

    public String GetCity() {
        return _city;
    }

    public String GetIdNum() {
        return _id_num;
    }

    public String GetFullId() {
        return String.format("%s%s", _city, _id_num);
    }

}
