/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Passenger client Logic
 */
package dfrsclient;

import RMIInterfaces.CommandResult;
import RMIInterfaces.Flight;
import RMIInterfaces.ICommonActions;
import RMIInterfaces.IPassengerActions;
import RMIInterfaces.SeatClass;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class PassengerUser implements IUser {

    private final HashMap<String, IPassengerActions> _destinations_stubs; //All the available server's stub
    private final ArrayList<String> _destination_names; //Stores the cities to construct a choosable destination string

    public PassengerUser(Registry r) throws RemoteException, NotBoundException, Exception {
        _destinations_stubs = new HashMap<>();
        _destination_names = new ArrayList<>();

        String[] servers = r.list();
        if (servers.length == 0) {
            throw new Exception("No servers available");
        } else {
            for (String s : servers) {
                _destinations_stubs.put(s, (IPassengerActions) r.lookup(s));
                _destination_names.add(s);
            }
        }

    }

    @Override
    public void MainInterfaceLoop() {

        IPassengerActions currentStub;
        Scanner s = new Scanner(System.in);
        String destChoice = "";
        int res, dest;
        String fn, ln, phone, addr, dateStr = null;
        DateTime date;
        SeatClass sc;
        DateTimeFormatter dtFormatter = DateTimeFormat.forPattern("yyyy-MM-dd h:mm a");

        //create a string in the form : (0) - MTL, (1) - WSH to have a fixed destination/departure value
        for (int i = 0; i < _destination_names.size(); ++i) {
            destChoice += String.format("(%d) - %s", i, _destination_names.get(i));
            if (i < _destination_names.size() - 1) {
                destChoice += ", ";
            }
        }

        try {
            do {
                System.out.println("Where are you leaving from : " + destChoice + "?");
                res = s.nextInt();
                s.nextLine();
            } while (res < 0 || res >= _destination_names.size());

            currentStub = _destinations_stubs.get(_destination_names.get(res));
            CommandResult<ArrayList<Flight>> flightRes = ((ICommonActions) currentStub).GetFlights();

            System.out.println("Here are the flights from " + _destination_names.get(res) + ":");
            if (flightRes.GetIsSuccess()) {
                ArrayList<Flight> flights = flightRes.GetResutlt();
                for (int i = 0; i < flights.size(); ++i) {
                    System.out.printf("%d - %s%n", i + 1, flights.get(i));
                }
            }
            System.out.println("------");

            do {
                System.out.println("Where do you want to go : " + destChoice + "?");
                dest = s.nextInt();
                s.nextLine();
            } while (res < 0 || res >= _destination_names.size());

            boolean dateValid = false;
            while (!dateValid) {
                System.out.println("When do you want to leave? (format :  2016-01-01 5:00 pm)");
                dateStr = s.nextLine();
                try{
                    dtFormatter.parseDateTime(dateStr);
                    dateValid = true;
                }catch(Exception ex){
                    dateValid = false;
                }
            }

            String seatClassChoice = "";
            SeatClass[] values = SeatClass.values();
            for (int i = 0; i < values.length; ++i) {
                seatClassChoice += String.format("(%d) - %s", i, values[i]);
                if (i < values.length - 1) {
                    seatClassChoice += ", ";
                }
            }

            do {
                System.out.println("What class do you want to be in : " + seatClassChoice + "?");
                res = s.nextInt();
                s.nextLine();
            } while (res < 0 || res >= values.length);
            sc = values[res];

            System.out.println("What is your first name?");
            fn = s.nextLine();

            System.out.println("What is your last name?");
            ln = s.nextLine();

            System.out.println("What is your phone number?");
            phone = s.nextLine();

            System.out.println("What is your address?");
            addr = s.nextLine();

            CommandResult<Object> bookRes = currentStub.BookFlight(fn, ln, addr, phone, _destination_names.get(dest), dtFormatter.parseDateTime(dateStr), sc);

            if (bookRes.GetIsSuccess()) {
                System.out.println("Flight booked!");
            } else {
                System.out.println("There was an error while booking your flight : " + bookRes.GetMessage());
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

}
