/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Main client program driver
 * 
 */
package dfrsclient;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class DFRSClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            String registryAddr;
            int registryPort;
            Registry reg;
            Scanner s = new Scanner(System.in);

            System.out.println("What is the address of the registry : ");
            registryAddr = s.nextLine();
            System.out.println("What is the port of the registry : ");
            registryPort = s.nextInt();
            reg = LocateRegistry.getRegistry(registryAddr, registryPort);

            IUser currentUser = null;
            int res;
            do {
                System.out.println("Are you a (1)Passenger or a (2)Manager : ");
                res = s.nextInt();
                s.nextLine();
                if (res == 1) {
                    try {
                        currentUser = new PassengerUser(reg);
                    } catch (Exception ex) {
                        System.out.println("Service unavailable : " + ex.getMessage());
                    }

                } else if (res == 2) {
                    ManagerId mId = null;
                    do {
                        String mIdString;
                        System.out.println("What is your manager ID : ");
                        mIdString = s.nextLine();
                        mId = ManagerId.GetFromFullId(mIdString);
                    } while (mId == null);

                    try {
                        currentUser = new ManagerUser(reg, mId);
                    } catch (Exception ex) {
                        System.out.println("Can't locate server for manager : " + ex.getMessage());
                    }

                }
            } while (res < 1 || res > 2);
            System.out.println("-----------");
            if (currentUser != null) {
                currentUser.MainInterfaceLoop();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

}
