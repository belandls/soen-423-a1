/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Manager client Logic
 */

package dfrsclient;

import RMIInterfaces.CommandResult;
import RMIInterfaces.FieldName;
import RMIInterfaces.Flight;
import RMIInterfaces.ICommonActions;
import RMIInterfaces.IManagerActions;
import RMIInterfaces.Logger;
import RMIInterfaces.ManagerParameter;
import RMIInterfaces.RecordType;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Scanner;

public class ManagerUser implements IUser {

    private ManagerId _manager_id;
    private IManagerActions _destination_stub; //The target server depending on the manager's id
    private Logger _log;

    public ManagerUser(Registry reg, ManagerId managerId) throws NotBoundException, RemoteException {
        _manager_id = managerId;
        _destination_stub = (IManagerActions) reg.lookup(_manager_id.GetCity());
        _log = new Logger("Client", "ManagerLog");
    }

    //Ask a question and get an integer value in a certain range
    private int GetResultFromKeyboard(String question, int range) {
        Scanner s = new Scanner(System.in);
        int res = -1;
        do {
            System.out.println(question);
            res = s.nextInt();
            s.nextLine();
        } while (res < 0 || res > range);
        return res;
    }

    //Ask a question and get a string result
    private String GetResultFromKeyboard(String question) {
        Scanner s = new Scanner(System.in);
        String res;
        do {
            System.out.println(question);
            res = s.nextLine();
        } while (res.length() == 0);
        return res;
    }

    //Main interface logic
    @Override
    public void MainInterfaceLoop() {
        try {
            int res = -1;
            boolean shouldExit = false;
            while (!shouldExit) {
                res = GetResultFromKeyboard("What do you want to do?\n(0) - Get Booked Flight Count\n(1) - Edit Flight\n(2) - Exit", 2);
                if (res == 0) {
                    res = GetResultFromKeyboard("What type or record?\n(0) - All\n(1) - ECON only\n(2) - BUSINESS only\n(3) - FIT ONLY", 3);
                    RecordType type = RecordType.NONE;
                    switch (res) {
                        case 0:
                            type = RecordType.ALL_BOOKED_FLIGHT;
                            break;
                        case 1:
                            type = RecordType.ECON_BOOKED_FLIGHT;
                            break;
                        case 2:
                            type = RecordType.BUSINESS_BOOKED_FLIGHT;
                            break;
                        case 3:
                            type = RecordType.FIT_BOOKED_FLIGHT;
                            break;
                    }
                    CommandResult<String> countResult = _destination_stub.GetBookedFlightCount(new ManagerParameter<>(_manager_id.GetIdNum(),type));
                    if (countResult.GetIsSuccess()) {
                        System.out.println("Here are the counts : " + countResult.GetResutlt());
                        _log.Log(_manager_id.GetFullId(), String.format("Retrieved booked flight count(%s) : %s.", type, countResult.GetResutlt()));
                    } else {
                        System.err.println("There was an error getting the cooked count : " + countResult.GetMessage());
                        _log.Log(_manager_id.GetFullId(), String.format("Unable to retrieve the booked flight count(%s) : %s.",type , countResult.GetMessage()));
                    }
                } else if (res == 1) { //Edit
                    CommandResult<ArrayList<Flight>> flightRes = ((ICommonActions) _destination_stub).GetFlights();

                    System.out.println("Here are the flights from " + _manager_id.GetCity() + ":");
                    if (flightRes.GetIsSuccess()) {
                        ArrayList<Flight> flights = flightRes.GetResutlt();
                        for (int i = 0; i < flights.size(); ++i) {
                            System.out.printf("%d - %s%n", i + 1, flights.get(i));
                        }
                    }
                    System.out.println("------");
                    int rId = GetResultFromKeyboard("Which flight(ID) do you want to modify (0 to create)?", 1000);
                    if (rId == 0) {
                        CommandResult<Flight> editRes = _destination_stub.EditFlightRecord(new ManagerParameter<>(_manager_id.GetIdNum(),rId), FieldName.CREATE, null);
                        if (editRes.GetIsSuccess()) {
                            System.out.println("Record created successfully! Here is the new data : ");
                            System.out.println(editRes.GetResutlt());
                            _log.Log(_manager_id.GetFullId(), "Created empty flight : " + editRes.GetResutlt());
                        } else {
                            _log.Log(_manager_id.GetFullId(), "Unable to create a new flight: " + editRes.GetMessage());
                            System.err.println("Could not create the record : " + editRes.GetMessage());
                        }
                    } else {
                        res = GetResultFromKeyboard("What do you want to modify?\n(0) - Destination\n(1) - Date\n(2) - ECON seats\n(3) - BUSINESS seats\n(4) - FIT seats\n(5) - Delete", 5);
                        if (res <= 5) {
                            FieldName f = null;
                            switch (res) {
                                case 0:
                                    f = FieldName.DESTINATION;
                                    break;
                                case 1:
                                    f = FieldName.DATE;
                                    break;
                                case 2:
                                    f = FieldName.ECON_SEATS;
                                    break;
                                case 3:
                                    f = FieldName.BUSINESS_SEATS;
                                    break;
                                case 4:
                                    f = FieldName.FIT_SEATS;
                                    break;
                                case 5:
                                    f = FieldName.DELETE;
                                    break;
                            }

                            String value = null;
                            if (f != FieldName.DELETE) {
                                value = GetResultFromKeyboard("What is the new value?");
                            }
                            CommandResult<Flight> editRes = _destination_stub.EditFlightRecord(new ManagerParameter<>(_manager_id.GetIdNum(),rId), f, value);
                            if (editRes.GetIsSuccess()) {
                                if (f == FieldName.DELETE) {
                                    System.out.println("Record deleted successfully! Here is the flight that was deleted : " + editRes.GetResutlt());
                                    _log.Log(_manager_id.GetFullId(), "Deleted flight : " + editRes.GetResutlt());
                                } else {
                                    System.out.println("Record edited successfully! Here is the new data : ");
                                    _log.Log(_manager_id.GetFullId(), String.format("Edited flight (%s -> %s) : %s.", f, value,editRes.GetResutlt()));
                                }

                                System.out.println(editRes.GetResutlt());
                            } else if (f == FieldName.DELETE) {
                                System.err.println("Could not delete the record : " + editRes.GetMessage());
                                _log.Log(_manager_id.GetFullId(), "Unable to delete flight("+rId+") : " + editRes.GetMessage());
                            } else {
                                System.err.println("Could not edit the record : " + editRes.GetMessage());
                                _log.Log(_manager_id.GetFullId(), "Unable to edit flight("+rId+") : " + editRes.GetMessage());
                            }
                        }
                    }

                } else if (res == 2) {
                    shouldExit = true;
                }
            }
        } catch (Exception ex) {
            System.err.println("The operation could not carry out. Is the server alive? " + ex.getMessage());
        }

    }

}
