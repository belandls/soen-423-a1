/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dfrstester;

import RMIInterfaces.FieldName;
import RMIInterfaces.IManagerActions;
import RMIInterfaces.IPassengerActions;
import RMIInterfaces.ManagerParameter;
import RMIInterfaces.RecordType;
import RMIInterfaces.SeatClass;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.HashMap;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Samuel
 */
public class DFRSTester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        DateTimeFormatter dtFormatter = DateTimeFormat.forPattern("yyyy-MM-dd h:mm a");
        Registry reg = LocateRegistry.getRegistry("localhost", 12345);
        HashMap<String, IPassengerActions> _destinations_stubs = new HashMap<>();
        HashMap<String, IManagerActions> _destinations_stubs_manager = new HashMap<>();
        String[] servers = reg.list();
        if (servers.length == 0) {
            throw new Exception("No servers available");
        } else {
            for (String s : servers) {
                _destinations_stubs.put(s, (IPassengerActions) reg.lookup(s));
                _destinations_stubs_manager.put(s, (IManagerActions) reg.lookup(s));
            }
        }

//        for (int i = 1; i <= 100; ++i) {
//            System.out.println("Booking...");
//            System.out.println(_destinations_stubs.get("MTL").BookFlight("Samuel", "BL", "an addr.", "111-222-3333", "ND", dtFormatter.parseDateTime("2012-01-01 2:00 pm"), SeatClass.FIT).GetMessage());
//            System.out.println(_destinations_stubs.get("WSH").BookFlight("Samuel", "BL", "an addr.", "111-222-3333", "ND", dtFormatter.parseDateTime("2012-01-01 2:00 pm"), SeatClass.FIT).GetMessage());
//            Thread.sleep(2000);
//        }
//        for (int i = 1; i <= 100; ++i) {
//            System.out.println(_destinations_stubs_manager.get("MTL").GetBookedFlightCount(new ManagerParameter<>("1212", RecordType.ALL_BOOKED_FLIGHT)).GetResutlt());
//            Thread.sleep(250);
//        }
//        for (int i = 1; i <= 100; ++i) {
//            System.out.println(_destinations_stubs_manager.get("MTL").GetBookedFlightCount(new ManagerParameter<>("1212", RecordType.ECON_BOOKED_FLIGHT)).GetResutlt());
//            Thread.sleep(250);
//        }
//        for (int i = 1; i <= 100; ++i) {
//            System.out.println(_destinations_stubs_manager.get("MTL").EditFlightRecord(new ManagerParameter<>("2211",-1), FieldName.CREATE, null).GetMessage());
//        }
//        _destinations_stubs_manager.get("MTL").EditFlightRecord(new ManagerParameter<>("2211", 100), FieldName.ECON_SEATS, "100");
//        _destinations_stubs_manager.get("MTL").EditFlightRecord(new ManagerParameter<>("2211", 99), FieldName.ECON_SEATS, "100");
//        _destinations_stubs_manager.get("MTL").EditFlightRecord(new ManagerParameter<>("2211", 100), FieldName.DESTINATION, "WSH");
        
//        for (int i = 2; i <= 102; ++i) {
//            System.out.println(_destinations_stubs_manager.get("MTL").EditFlightRecord(new ManagerParameter<>("2211", -1), FieldName.CREATE, null).GetResutlt());
//            Thread.sleep(2000);
//        }
//        
//        for (int i = 2; i <= 102; ++i) {
//            System.out.println(_destinations_stubs_manager.get("MTL").EditFlightRecord(new ManagerParameter<>("2211", i), FieldName.DELETE, null).GetResutlt());
//            Thread.sleep(2000);
//        }
//        
//         for (int i = 1; i <= 100; ++i) {
//            System.out.println("Booking...");
//            System.out.println(_destinations_stubs.get("MTL").BookFlight("Samuel", "BL", "an addr.", "111-222-3333", "WSH", dtFormatter.parseDateTime("1900-01-01 12:00 AM"), SeatClass.ECON).GetMessage());
//            Thread.sleep(2000);
//        }
         
//         for (int i = 1; i <= 100; ++i) {
//            System.out.println("editing...");
//            System.out.println(_destinations_stubs_manager.get("MTL").EditFlightRecord(new ManagerParameter<>("2211", 99), FieldName.FIT_SEATS, new Integer(i).toString()).GetResutlt());
//            Thread.sleep(2000);
//        }

    }

}
