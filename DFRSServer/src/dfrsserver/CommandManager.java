/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Main server object. Handles all the RMI calls
 */

package dfrsserver;

import RMIInterfaces.CommandResult;
import RMIInterfaces.FieldName;
import RMIInterfaces.Flight;
import RMIInterfaces.ICommonActions;
import RMIInterfaces.IHostRepository;
import RMIInterfaces.IManagerActions;
import RMIInterfaces.IPassengerActions;
import RMIInterfaces.Logger;
import RMIInterfaces.ManagerParameter;
import RMIInterfaces.RecordType;
import RMIInterfaces.RegisteredHost;
import RMIInterfaces.SeatClass;
import java.net.InetAddress;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.joda.time.DateTime;


public class CommandManager extends UnicastRemoteObject implements IPassengerActions, IManagerActions, ICommonActions, IUDPCallback, IHostRepository {

    private BookingManager _bm; //to handle booking logic
    private String _current_city;
    private UDPHandler _udp_handler; //to handle UDP send/receive
    private HashMap<String, RegisteredHost> _host_port_map; //Used for the UDP registry if this is the first server
    private IHostRepository _registered_host_stub;
    private int _expected_udp_answers;
    private int _received_udp_answers;
    private String _field_count_result;
    private final Object _flight_count_mutex = new Object();

    public CommandManager(String currentCity, int port, IHostRepository registeredHostStub) throws RemoteException, SocketException {
        super();
        //if this is the first server, it becomes the UDP registry. Otherwise, a stub to the UDP registry server is passed
        if (registeredHostStub == null) {
            _host_port_map = new HashMap<>();
            try {
                //The server registers it's UDP information to be accessed by the other servers
                RegisteredHost temp = new RegisteredHost(InetAddress.getLocalHost().getHostAddress(), port);
                _host_port_map.put(currentCity, temp);
            } catch (Exception ex) {

            }
        }
        _registered_host_stub = registeredHostStub;
        _bm = new BookingManager(currentCity);
        _udp_handler = new UDPHandler(port);
        _udp_handler.AttachOnCommandReceive(this); //Attaches the callback for when a UDPMessage is received
        _udp_handler.StartListening();
        _expected_udp_answers = 0;
        _received_udp_answers = 0;
        _field_count_result = "";
        _current_city = currentCity;

    }

    private synchronized void AppendFieldCountResult(String city, int count) {
        if (_field_count_result.length() > 0) {
            _field_count_result += ", ";
        }
        _field_count_result += String.format("%s: %d", city, count);
    }

    @Override
    public Flight Test() throws RemoteException {
        return new Flight("MTL", "ND", DateTime.now(), 0, 0, 0);
    }

    @Override
    public CommandResult<ArrayList<Flight>> GetFlights() throws RemoteException {
        return new CommandResult<>(true, "", _bm.GetFlights());
    }

    @Override
    public CommandResult<Object> BookFlight(String fn, String ln, String addr, String phone, String dest, DateTime date, SeatClass sc) throws RemoteException {
        Passenger pTemp = new Passenger(fn, ln, addr, phone);
        String res = _bm.TryToBookAFlight(pTemp, dest, date, sc);
        if (res == null) {
            return new CommandResult<>(true, res, null);
        } else {
            Logger.GlobalInstance.Log("Passenger", String.format("Couldn't book flight to %s at %s in %s for %s. %s/", dest, date, sc, pTemp, res));
            return new CommandResult<>(false, res, null);
        }
    }

    @Override
    public CommandResult<String> GetBookedFlightCount(ManagerParameter<RecordType> type) {
        synchronized (_flight_count_mutex) {
            _expected_udp_answers = 0;
            _received_udp_answers = 0;
            _field_count_result = "";
            try {
                ArrayList<RegisteredHost> rHosts;
                //GEt the UDP information for the other servers
                if (_registered_host_stub == null) {
                    rHosts = this.GetAllRegisteredHostExceptCalling(_current_city);
                } else {
                    rHosts = _registered_host_stub.GetAllRegisteredHostExceptCalling(_current_city);
                }
                _expected_udp_answers = rHosts.size(); //Set the number of expected packets
                //Send the packet to each
                for (RegisteredHost rh : rHosts) {
                    Logger.GlobalInstance.Log(_current_city + type.GetManagerId(), String.format("Requesting booked flight count to %s:%d server through UDP.", rh.GetHost(), rh.GetPort()));
                    UDPMessage temp = new UDPMessage(UDPMessageType.GET_RECORD, _current_city, type.GetParameterValue(), type.GetManagerId());
                    _udp_handler.SendMessage(rh.GetHost(), rh.GetPort(), temp);
                }

                //Count this server Booked count
                AppendFieldCountResult(_current_city, _bm.GetBookingCount(type.GetParameterValue()));

                //wait for other server's answer
                while (_expected_udp_answers != _received_udp_answers) {
                    Thread.sleep(5);
                }

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
            return new CommandResult<String>(true, _current_city, _field_count_result);
        }
    }

    //Callback when UDP message gets in.
    @Override
    public void MessageReceived(UDPMessage comm, InetAddress srcHost, int srcPort) {
        if (comm.GetMessageType() == UDPMessageType.GET_RECORD) {
            Logger.GlobalInstance.Log(comm.GetSrc() + comm.GetCallingManagerID(), String.format("Received request from %s to get flight count for %s.", comm.GetSrc(), comm.GetRecordType()));
            int bf = _bm.GetBookingCount(comm.GetRecordType());
            UDPMessage temp = new UDPMessage(UDPMessageType.GET_RECORD_RESULT, _current_city, comm.GetRecordType(), bf, comm.GetCallingManagerID());
            _udp_handler.SendMessage(srcHost, srcPort, temp);
            
        } else if (comm.GetMessageType() == UDPMessageType.GET_RECORD_RESULT) {
            Logger.GlobalInstance.Log(_current_city + comm.GetCallingManagerID(), String.format("Received %s(%d) from %s.", comm.GetRecordType(), comm.GetCount(), comm.GetSrc()));
            AppendFieldCountResult(comm.GetSrc(), comm.GetCount());
            ++_received_udp_answers;
        }
    }

    @Override
    public void Register(String city, int port) throws RemoteException {
        //Only allow UDP registration if the current server is the UDP registry server 
        if (_host_port_map != null) {
            Logger.GlobalInstance.Log(city, String.format("Registring port %d for UDP communication for city : %s.", port, city));
            try {
                RegisteredHost temp = new RegisteredHost(RemoteServer.getClientHost(), port);
                _host_port_map.put(city, temp);
            } catch (Exception ex) {

            }

        }
    }

    //GEt UDP info for a specific city
    @Override
    public RegisteredHost GetHostInformation(String targetCity) throws RemoteException {
        if (_host_port_map != null) { //check if this is the UDP registry server
            try {
                Logger.GlobalInstance.Log(RemoteServer.getClientHost(), String.format("Retrieving UDP port for city : %s.", targetCity));
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
            return _host_port_map.get(targetCity);
        }
        return null;
    }

    //GEt UDP info for all other cities
    @Override
    public ArrayList<RegisteredHost> GetAllRegisteredHostExceptCalling(String callingCity) throws RemoteException {
        if (_host_port_map != null) {
            Logger.GlobalInstance.Log(callingCity, "Retrieving all registered host.");
            ArrayList<RegisteredHost> res = new ArrayList<>();
            for (Map.Entry<String, RegisteredHost> e : _host_port_map.entrySet()) {
                if (!e.getKey().equals(callingCity)) {
                    res.add(e.getValue());
                }
            }
            return res;
        }
        return null;
    }

    @Override
    public CommandResult<Flight> EditFlightRecord(ManagerParameter<Integer> recordId, FieldName fn, String value) throws RemoteException {
        String resultMessage = null;
        Flight affectedFlight = null;
        if (fn == FieldName.CREATE) {
            Flight temp = _bm.CreateEmptyFlight();
            Logger.GlobalInstance.Log(_current_city + recordId.GetManagerId(), String.format("Created new empty flight : %s.", temp));
            return new CommandResult<>(true, null, temp);
        } else {
            affectedFlight = _bm.GetFlightFromId(recordId.GetParameterValue());
            if (affectedFlight != null) {
                if (fn == FieldName.DELETE) {
                    resultMessage = _bm.TryToDeleteFlight(affectedFlight);
                    if (resultMessage == null) {
                        Logger.GlobalInstance.Log(_current_city + recordId.GetManagerId(), String.format("Deleted flight : %s.", affectedFlight));
                    }
                } else {
                    resultMessage = affectedFlight.UpdateRecord(fn, value);
                    if (resultMessage == null) {
                        Logger.GlobalInstance.Log(_current_city + recordId.GetManagerId(), String.format("Updated %s on flight {%s} to %s.", fn, affectedFlight, value));
                    }
                }
            } else {
                resultMessage = String.format("There are no flights with the id %d", recordId.GetParameterValue());
            }
            if (resultMessage != null) {
                Logger.GlobalInstance.Log(_current_city + recordId.GetManagerId(), String.format("Error editing the flight {%s}.%n%s", affectedFlight,resultMessage));
            }
        }
        return new CommandResult<>(resultMessage == null, resultMessage, affectedFlight);
    }

}
