/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Represents the passenger's information
 */
package dfrsserver;

/**
 *
 * @author Samuel
 */
public class Passenger {
    
    private static int _Record_Id_Counter = 0;
    
    private final int _record_id;
    
    private final String _first_name;
    private final String _last_name;
    private final String _addr;
    private final String _phone;
    
    public Passenger(String fn, String ln, String addr, String phone){
        _record_id = ++_Record_Id_Counter;
        _first_name = fn;
        _last_name = ln;
        _addr = addr;
        _phone = phone;
    }

    public String GetFirstName(){
        return _first_name;
    }
    
    public String GetLastName(){
        return _last_name;
    }
    
    public String GetAddr() {
        return _addr;
    }
    
    public String GetPhone(){
        return _phone;
    }
    
    @Override
    public String toString(){
        return String.format("%s, %s (%s) phone:%s", _last_name, _first_name, _addr, _phone);
    }
}
