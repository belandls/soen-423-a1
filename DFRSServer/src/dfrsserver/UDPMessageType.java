/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Possible messages sent/received using UDP
 */
package dfrsserver;

/**
 *
 * @author Samuel
 */
public enum UDPMessageType {
    GET_RECORD,
    GET_RECORD_RESULT
}
