/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Main server driver
 */
package dfrsserver;

import RMIInterfaces.IHostRepository;
import RMIInterfaces.IPassengerActions;
import RMIInterfaces.Logger;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 *
 * @author Samuel
 */
public class DFRSServer {

    
    public static void main(String[] args) throws RemoteException {

        if (args.length != 3) {
            System.out.println("You need to provide 3 parameters : 1 : Should create RMI registry (1/0), 2: Current location (Ex.MTL), 3:UDP listening port");
            return;
        }

        Logger l = new Logger(args[1], args[1]);
        Logger.GlobalInstance = l;

        int UDPListenPort = Integer.parseInt(args[2]);
        Registry r = null;
        try {
            
            if (args[0].equals("1")) {
                //Create registry if parameter is set
                r = LocateRegistry.createRegistry(12345);
            } else {
                //Else ask where the registry is to bind this server to it
                String registryAddr;
                int registryPort;
                Scanner s = new Scanner(System.in);

                System.out.println("What is the address of the registry : ");
                registryAddr = s.nextLine();
                System.out.println("What is the port of the registry : ");
                registryPort = s.nextInt();
                r = LocateRegistry.getRegistry(registryAddr, registryPort);
            }
            String[] otherServers = r.list();
            IHostRepository tempStub = null;
            if (otherServers.length > 0) {
                //If not the first server, register UDP info with UDP reistry
                String registeredHostServer = otherServers[0];
                tempStub = (IHostRepository) r.lookup(registeredHostServer);
                tempStub.Register(args[1], UDPListenPort);
            }
            CommandManager cm = new CommandManager(args[1], UDPListenPort, tempStub);
            r.rebind(args[1], cm); //Bind the server
            System.out.println("Server " + args[1] + " is ready");
            l.Log(args[1], "Server " + args[1] + " is ready");
        } catch (Exception ex) {
            System.out.println("There was an error binding the server to the registry : " + ex.getMessage());
            l.Log("", "There was an error binding the server to the registry : " + ex.getMessage());
        }

    }

}
