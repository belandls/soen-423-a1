/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Defines a callback function for when a UDP Message is received
 */
package dfrsserver;

import java.net.InetAddress;

/**
 *
 * @author Samuel
 */
public interface IUDPCallback {
    public void MessageReceived(UDPMessage comm, InetAddress srcHost, int srcPort);
}
