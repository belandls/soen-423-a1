/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Handles all the flight/booking logic
 */

package dfrsserver;

import RMIInterfaces.Flight;
import RMIInterfaces.Logger;
import RMIInterfaces.RecordType;
import RMIInterfaces.SeatClass;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class BookingManager {

    private final ArrayList<Flight> _flights;
    private final HashMap<String, ArrayList<Booking>> _bookings;
    private final String _current_city;
    
    //To synchronize the flights and booking read/write operations
    private final ReentrantReadWriteLock _flights_rw_lock;
    private final Lock _flights_r_lock;
    private final Lock _flights_w_lock;
    private final ReentrantReadWriteLock _bookings_rw_lock;
   private final Lock _bookings_r_lock;
    private final Lock _bookings_w_lock;

    public BookingManager(String currentCity) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        _flights = new ArrayList<>();
        //creates dummy initial flights
        _flights.add(new Flight(currentCity, "ND", formatter.parseDateTime("2012-01-01 14:00:00"), 1, 20, 30));
        _flights.add(new Flight(currentCity, "WSH", formatter.parseDateTime("2012-02-01 15:00:00"), 1, 20, 30));
        _bookings = new HashMap<>();
        _current_city = currentCity;
        _flights_rw_lock = new ReentrantReadWriteLock();
        _flights_r_lock = _flights_rw_lock.readLock();
        _flights_w_lock = _flights_rw_lock.writeLock();
        _bookings_rw_lock = new ReentrantReadWriteLock();
        _bookings_r_lock = _bookings_rw_lock.readLock();
        _bookings_w_lock = _bookings_rw_lock.writeLock();
    }

    public String TryToBookAFlight(Passenger p, String dest, DateTime date, SeatClass sc) {

        Flight currentFlight = null;

        _flights_r_lock.lock();
        try {
            for (Flight f : _flights) {
                if (f.Equals(dest, date)) {
                    currentFlight = f;
                    break;
                }
            }
        } finally {
            _flights_r_lock.unlock();
        }

        if (currentFlight != null) {
            if (currentFlight.TryToTakeSeat(sc)) {
                _bookings_w_lock.lock();
                try {
                    String lastNameFirstLetter = p.GetLastName().substring(0, 1);
                    if (!_bookings.containsKey(lastNameFirstLetter)) {
                        _bookings.put(lastNameFirstLetter, new ArrayList<>());
                    }
                    ArrayList<Booking> lst = _bookings.get(lastNameFirstLetter);
                    Booking temp = new Booking(p, currentFlight, sc);
                    lst.add(temp);
                    Logger.GlobalInstance.Log(String.format("%s, %s", p.GetLastName(), p.GetFirstName()),String.format("Booked flight : %s", temp));
                } finally {
                    _bookings_w_lock.unlock();
                }
            } else {
                return String.format("There are no more %s seats on the flight : %s", sc, currentFlight);
            }
        } else {
            return "No flight matching the descrition was found";
        }
        return null;
    }

    public ArrayList<Flight> GetFlights() {
        return _flights;
    }

    public Flight GetFlightFromId(int id) {
        //To avoid add/delete during iteration

        _flights_r_lock.lock();
        try {
            for (Flight f : _flights) {
                if (f.GetRecordId() == id) {
                    return f;
                }
            }
        } finally {
            _flights_r_lock.unlock();
        }

        return null;
    }

    public Flight CreateEmptyFlight() {
        Flight temp = new Flight(_current_city, _current_city, new DateTime(1900, 1, 1, 0, 0), 0, 0, 0);

        //To ensure proper iteration over the flight collection
        _flights_w_lock.lock();
        try {
            _flights.add(temp);
        } finally {
            _flights_w_lock.unlock();
        }

        return temp;
    }

    public String TryToDeleteFlight(Flight f) {
        //Check if passengers already booked some flights
        boolean hasBookedFlights = false;
        _bookings_r_lock.lock();
        try {
            //Check if the flight has any bookings first
            for (Map.Entry<String, ArrayList<Booking>> bl : _bookings.entrySet()) {
                for (Booking b : bl.getValue()) {
                    if (b.GetAssociatedFlight().Equals(f)) {
                        hasBookedFlights = true;
                        break;
                    }
                }
                if (hasBookedFlights) {
                    break;
                }
            }
        } finally {
            _bookings_r_lock.unlock();
        }
        if (hasBookedFlights) {
            return "There is at least one booking for this flight";
        }

        //To avoid add/delete during iteration
        _flights_w_lock.lock();
        try {
            _flights.remove(f); //f should already be a ref from _flights
        } finally {
            _flights_w_lock.unlock();
        }

        return null;
    }

    public int GetBookingCount(RecordType type) {
        int count = 0;
        _bookings_r_lock.lock();
        try {
            for (Map.Entry<String, ArrayList<Booking>> bl : _bookings.entrySet()) {
                switch (type) {
                    case ALL_BOOKED_FLIGHT:
                        count += bl.getValue().size();
                        break;
                    case BUSINESS_BOOKED_FLIGHT:
                        for (Booking b : bl.getValue()) {
                            if (b.GetSeatClass() == SeatClass.BUSINESS) {
                                ++count;
                            }
                        }
                        break;
                    case ECON_BOOKED_FLIGHT:
                        for (Booking b : bl.getValue()) {
                            if (b.GetSeatClass() == SeatClass.ECON) {
                                ++count;
                            }
                        }
                        break;
                    case FIT_BOOKED_FLIGHT:
                        for (Booking b : bl.getValue()) {
                            if (b.GetSeatClass() == SeatClass.FIT) {
                                ++count;
                            }
                        }
                        break;
                    default:
                        break;
                }

            }
        } finally {
            _bookings_r_lock.unlock();
        }
        return count;
    }
}
