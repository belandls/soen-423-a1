/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Simple object to ease sending/receiving information over UDP
 */
package dfrsserver;

import RMIInterfaces.RecordType;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author Samuel
 */
public class UDPMessage implements Serializable{

    private String _src;
    private UDPMessageType _mess_type;
    private String _calling_manager_id;
    private RecordType _rec_type;
    private int _count;

    public UDPMessage(UDPMessageType mType, String src, RecordType rType, String callingManagerId) {
        this(mType, src, rType, 0, callingManagerId);
    }

    public UDPMessage(String src, int count, RecordType rType, String callingManagerId) {
        this(UDPMessageType.GET_RECORD_RESULT, src, rType, count, callingManagerId);
    }

    public UDPMessage(UDPMessageType mType, String src, RecordType rType, int count, String callingManagerId) {
        _mess_type = mType;
        _src = src;
        _rec_type = rType;
        _count = count;
        _calling_manager_id = callingManagerId;
    }

    public String GetSrc(){
        return _src;
    }
    
    public UDPMessageType GetMessageType(){
        return _mess_type;
    }
    
    public String GetCallingManagerID(){
        return _calling_manager_id;
    }
    
    public RecordType GetRecordType(){
        return _rec_type;
    }
    
    public int GetCount(){
        return _count;
    }
    public byte[] ToBytes() {
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            ObjectOutputStream o = new ObjectOutputStream(b);
            o.writeObject(this);
            byte[] res = b.toByteArray();
            o.close();
            return res;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    public static UDPMessage FromBytes(byte[] data) {
         try {
             ByteArrayInputStream b = new ByteArrayInputStream(data);
             ObjectInputStream o = new ObjectInputStream(b);
            UDPMessage res = (UDPMessage)o.readObject();
            o.close();
            return res;
        } catch (Exception ex) {

        }
        return null;
    }
}
