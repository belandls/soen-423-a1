/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Sends/Receives UDP Datagrams
 */
package dfrsserver;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;


public class UDPHandler implements Runnable {

    private static final int BUFFER_SIZE = 2048;

    private IUDPCallback _command_event_listener;
    private boolean _should_listen;
    private Thread _associated_thread;
    private DatagramSocket _socket;

    public UDPHandler(int port) throws SocketException {
        //create the soclet to listen (and eventually send) on the desired port
        _socket = new DatagramSocket(port);
    }

    //to attach the callback (limited to 1 listener for simplicity)
    public void AttachOnCommandReceive(IUDPCallback src) {
        _command_event_listener = src;
    }

    //Start the thread that constantly receives
    public void StartListening() {
        if (!_should_listen) {
            if (_associated_thread != null) {
                try {
                    _associated_thread.join();
                } catch (Exception ex) {

                }
            }
            _should_listen = true;
            _associated_thread = new Thread(this);
            _associated_thread.start();
        }

    }

    //Sets the condition to false so that the thread finishes cleanly
    public void StopListening() {
        _should_listen = false;
    }

    public boolean SendMessage(InetAddress host, int port, UDPMessage message) {
        try {
            byte[] data = message.ToBytes();
            DatagramPacket p = new DatagramPacket(data, data.length, host, port);
            _socket.send(p);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public boolean SendMessage(String host, int port, UDPMessage message) {
        try {
            InetAddress addr = InetAddress.getByName(host);
            return SendMessage(addr, port, message);
        } catch (Exception ex) {
            return false;
        }

    }

    //Receive loop
    @Override
    public void run() {
        while (_should_listen) {
            try {
                byte[] buffer = new byte[BUFFER_SIZE];
                DatagramPacket temp = new DatagramPacket(buffer, BUFFER_SIZE);
                _socket.receive(temp);
                UDPMessage recvMessage = UDPMessage.FromBytes(temp.getData());
                _command_event_listener.MessageReceived(recvMessage, temp.getAddress(), temp.getPort());
            } catch (Exception ex) {
                _should_listen = false;
            }
        }
    }

}
