/*
 * Author : Samuel Beland-Leblanc
 * Purpose : Agglomerates all the information of a booking into a single entity for simplicity and clarity
 */

package dfrsserver;

import RMIInterfaces.Flight;
import RMIInterfaces.SeatClass;

public class Booking {
    
    private static int _Record_Id_Counter = 0;
    
    private final int _record_id;
    private final Passenger _associated_passenger;
    private final Flight _associated_flight;
    private final SeatClass _seat_class;
    
    public Booking(Passenger p, Flight f, SeatClass sc){
        _record_id = ++_Record_Id_Counter;
        _associated_passenger = p;
        _associated_flight = f;
        _seat_class = sc;
    }
    
    public Passenger GetAssociatedPassenger(){
        return _associated_passenger;
    }
    
    public Flight GetAssociatedFlight(){
        return _associated_flight;
    }
    
    public SeatClass GetSeatClass(){
        return _seat_class;
    }
    
    @Override
    public String toString(){
        return String.format("[id:%d] %s booked in %s for %s", _record_id, _associated_passenger, _seat_class, _associated_flight);
    }
}
